angular.module('skyTramRoute', []);

angular.module('skyTramRoute').controller('RouteController', function($scope) {
	$scope.map = {
		leafLetMap: null,
		mapContainerId: 'map',
		center: {
			lat: 53.21297198477735, 
			lng: 50.13447761535644,
		},
		zoom: 14,
		minZoom: 1,
		tileUrl: 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
	};
	$scope.skyRoutes = [
		{
			routeName: 'demonstrativeRoute',
			coordinates: [[53.214308339141894, 50.162458419799805],
						[53.21955055757548, 50.1558494567871],
						[53.22001307546095, 50.156965255737305],
						[53.2222228142004, 50.15533447265625],
						[53.222685303229305, 50.15327453613281],
						[53.22176032017716, 50.15138626098633],
						[53.22475359582907, 50.14932632446289],
						[53.22530598005638, 50.14962673187256],
						[53.21952486199095, 50.13185977935791],
						[53.21189259120532, 50.11138916015625],
						[53.21004213895603, 50.108299255371094],
						[53.20649521542376, 50.10787010192871],
						[53.204683086831324, 50.1081919670105],
						[53.20285802881733, 50.10913610458373]],
			speed: [[2000],
					[2000],
					[2000],
					[2000],
					[2000],
					[2000],
					[2000],
					[2000],
					[2000],
					[2000],
					[2000],
					[2000],
					[2000],
					[2000]]
		},
	];
	$scope.copters = [
		{
			copterName: 'Phantom',
			copterObject: null
		}
	];
	

	$scope.initMap = function() {
		var greenIcon = L.icon({
    		iconUrl: 'images/marker-icon.png',

    		iconSize:     [50, 50], // size of the icon
    		iconAnchor:   [25, 25], // point of the icon which will correspond to marker's location
    		popupAnchor:  [0, 0] // point from which the popup should open relative to the iconAnchor
		});

		$scope.map.leafLetMap = new L.Map($scope.map.mapContainerId, {
			center: new L.LatLng($scope.map.center.lat, $scope.map.center.lng),
			zoom: $scope.map.zoom,
			minZoom: $scope.map.minZoom
		});
		$scope.map.leafLetMap.addLayer(new L.TileLayer($scope.map.tileUrl, {
    		attribution: 'Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
    		maxZoom: 18
		}));

		L.polyline($scope.getRoute('demonstrativeRoute').coordinates, {color: 'red'}).addTo($scope.map.leafLetMap);

		$scope.getCopter('Phantom').copterObject = L.Marker.movingMarker($scope.getRoute('demonstrativeRoute').coordinates, $scope.getRoute('demonstrativeRoute').speed, {autostart: true, icon: greenIcon, loop: true}).addTo($scope.map.leafLetMap);
		$scope.map.leafLetMap.fitBounds($scope.getRoute('demonstrativeRoute').coordinates);

		$scope.getCopter('Phantom').copterObject.on('end', function() {
    		/*$scope.getCopter('Phantom').copterObject.bindPopup('<b>Доставлено!</b>', {
    			closeOnClick: false
    		}).openPopup();
    		$scope.getCopter('Phantom').copterObject.start();*/
    	});
		//$scope.map.leafLetMap.on('click', $scope.onMapClick);
	};
	$scope.onMapClick = function(event) {
		var point = L.circle([event.latlng.lat, event.latlng.lng], 10, {
						color: 'red',
						fillColor: '#f03'
					}).addTo($scope.map.leafLetMap),
			popup = L.popup()
    			.setLatLng([event.latlng.lat, event.latlng.lng])
    			.setContent(event.latlng.lat + " " + event.latlng.lng)
    			.openOn($scope.map.leafLetMap);
	};
	$scope.getCopter = function(name) {
		for (var i = 0, maxi = $scope.copters.length; i < maxi; i++) {
			if ($scope.copters[i].copterName === name) return $scope.copters[i];
		}
	};
	$scope.getRoute = function(name) {
		for (var i = 0, maxi = $scope.skyRoutes.length; i < maxi; i++) {
			if ($scope.skyRoutes[i].routeName === name) return $scope.skyRoutes[i];
		}
	};
	$scope.simulate = function() {};

	$scope.initMap();
});
<!DOCTYPE>
<!-- saved from url=(0033)http://taskmate.ru/ui/tasks.html# -->
<html>
	<head>
		<title>Sky-tram route</title>
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
		<link href="css/leaflet.css" rel="stylesheet" type="text/css">
	</head>
	<body>
		<div style="width: 100%; height:500px" id="map" ng-app="skyTramRoute" ng-controller="RouteController">
		</div>
		<script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
		<script src="js/angular.min.js" type="text/javascript"></script>
		<script src="js/leaflet.js" type="text/javascript"></script>
		<script src="js/MovingMarker.js" type="text/javascript"></script>
		<script src="js/sky.tram.route.js" type="text/javascript"></script>
	</body>
</html>